<?php

namespace app\components\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogBehavior
 *
 * @author kas
 */
class LogBehavior extends Behavior {

    //put your code here
    public $ownername;

    function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterinsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterupdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterdelete',
        ];
    }

    function afterinsert() {
        Yii::info('Добавлен новый ' . $this->ownername.' c id = '.$this->owner->id, 'change_info');
    }

    function afterupdate() {
        Yii::info('Изменен ' . $this->ownername.' c id = '.$this->owner->id , 'change_info');
    }

    function afterdelete() {
        Yii::info('Удален ' . $this->ownername.' c id = '.$this->owner->id, 'change_info');
    }

}
