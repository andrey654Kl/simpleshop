<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Product;
use Yii;
use yii\filters\AccessControl;

/**
 * Description of PurchaseController
 *
 * @author kas
 */
class PurchaseController extends Controller {

    //put your code here
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['buy'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                    $user = Yii::$app->user->getIdentity();
                    if ($user && $user->isUser()) {
                        return true;
                    }
                    return false;
                }
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                    $user = Yii::$app->user->getIdentity();
                    if ($user && $user->isAdmin()) {
                        return true;
                    }
                    return false;
                }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Purchase::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBuy($productID) {
        $product = $this->findProduct($productID);
        $model = new \app\models\PurchaseForm(['productID' => $productID]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->buying();
            return $this->redirect(['/product/index']);
        } else {
            return $this->render('buying', [
                        'model' => $model,
                        'product' => $product
            ]);
        }
    }

    protected function findProduct($id) {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Данный товар не найден');
        }
    }

}
