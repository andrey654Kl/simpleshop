<?php

use yii\db\Migration;

class m161116_123717_add_user extends Migration
{
    public function up()
    {
           $this->createTable('user', [
               'id'=>$this->primaryKey(),
               'login'=>$this->string(255)->notNull(),
               'password'=>$this->string(255)->notNull(),
               'role'=>$this->integer()
           ]);
           $this->insert('user', [
               'login'=>'admin',
               'password'=>  \app\models\User::encryptPassword('admin'),
               'role'=>  \app\models\User::ROLE_ADMIN
           ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
