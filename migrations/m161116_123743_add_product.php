<?php

use yii\db\Migration;

class m161116_123743_add_product extends Migration {

    public function up() {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'price' => $this->decimal(9,2),
            'count'=>$this->integer()
        ]);
    }

    public function down() {
        $this->dropTable('product');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
