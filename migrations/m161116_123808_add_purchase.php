<?php

use yii\db\Migration;

class m161116_123808_add_purchase extends Migration {

    public function up() {
        $this->createTable('purchase', [
            'id' => $this->primaryKey(),
            'userID' => $this->integer()->notNull(),
            'productID'=>$this->integer()->notNull(),
            'date' => $this->timestamp(),
            'price' => $this->decimal(9, 2),
            'count' => $this->integer()
        ]);
    }

    public function down() {
        $this->dropTable('purchase');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
