<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property integer $userID
 * @property integer $productID
 * @property string $date
 * @property string $price
 * @property integer $count
 */
class Purchase extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'purchase';
    }

    function behaviors() {
        return [
            [
                'class' => \app\components\behaviors\LogBehavior::className(),
                'ownername'=>'покупка'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userID', 'productID'], 'required'],
            [['userID', 'productID', 'count'], 'integer'],
            [['date'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userID' => 'Пользователь',
            'productID' => 'Товар',
            'date' => 'Дата покупки',
            'price' => 'Цена',
            'count' => 'Количество',
        ];
    }

}
