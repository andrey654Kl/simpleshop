<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;

/**
 * Description of PurchaseForm
 *
 * @author kas
 */
class PurchaseForm extends \yii\base\Model {

    //put your code here
    public $productID;
    public $count;

    public function rules() {
        return [
            [['productID'], 'required'],
            [['productID'], 'integer'],
            [['productID'], 'validateCount'],
            [['productID'], 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id'],
            ['count', 'integer', 'min' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'productID' => 'Товар',
            'count' => 'Количество',
        ];
    }

    function validateCount($attribute, $params) {
        $product = Product::findOne($this->productID);
        if ($product->count < $this->count) {
            $this->addError($attribute, 'На складе недостаточно товаров');
        }
    }

    function buying() {
        if (!$this->validate()) {
            return false;
        } else {
            $product = Product::findOne($this->productID);
            $purchase = new Purchase();
            $purchase->userID = Yii::$app->user->id;
            $purchase->productID = $this->productID;
            $purchase->count = $this->count;
            $purchase->price = $this->count * $product->price;
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                Product::updateAllCounters(['count' => -$this->count], ['id' => $this->productID]);
                $purchase->save();
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
            Yii::info('Совершенна покупка товара  ' . $product->id . ' пользователем ' . $purchase->userID.' в количестве '.$this->count, 'change_info');
            return true;
        }
    }

}
