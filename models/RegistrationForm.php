<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use app\models\User;

/**
 * Description of RegistrationForm
 *
 * @author kas
 */
class RegistrationForm extends \yii\base\Model {

    //put your code here
    public $login;
    public $password;

    public function rules() {
        return [
            [['login', 'password'], 'required'],
            ['login', 'unique', 'targetClass' => User::className()],
        ];
    }
    
        public function attributeLabels()
    {
        return [
            'login' => 'Имя пользователя',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    public function reg() {
        if (!$this->validate()) {
            return false;
        } else {
            $user = new User();
            $user->login = $this->login;
            $user->password = User::encryptPassword($this->password);
            return $user->save();
        }
    }


}
