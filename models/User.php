<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $role
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    function behaviors() {
        return [
            [
                'class' => \app\components\behaviors\LogBehavior::className(),
                'ownername'=>'Пользователь'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['login', 'password'], 'required'],
            [['role'], 'integer'],
            [['login', 'password'], 'string', 'max' => 255],
            ['role', 'default', 'value' => self::ROLE_USER]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Имя пользователя',
            'password' => 'Пароль',
            'role' => 'Роль',
        ];
    }

    static function encryptPassword($password) {
        return md5($password);
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

    function isAdmin() {
        return $this->role == self::ROLE_ADMIN;
    }

    function isUser() {
        return $this->role == self::ROLE_USER;
    }

}
