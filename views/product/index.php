<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$user = Yii::$app->user->getIdentity();
$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= ($user && $user->isAdmin()) ?Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']):'' ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'price',
            'count',
            [
                'label' => 'Купить',
                'value' => function ($data) {
                    return Html::a('Купить', Url::to(['/purchase/buy', 'productID' => $data->id]));
                },
                        'format' => 'raw',
                        'visible'=>($user && $user->isUser())
                    ],
                    ['class' => 'yii\grid\ActionColumn', 'visible' => ($user && $user->isAdmin())],
                ],
            ]);
            ?>
</div>
