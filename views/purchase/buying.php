<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Покупка товара - ' . $product->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-buy">
    <h1><?= Html::encode($this->title) ?></h1>
    <div>
        Цена за единиицу товара : <?= $product->price ?>
    </div>
    <div>
        Текущее количество товаров на складе: <?= $product->count ?>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
    ]);
    ?>

    <?= $form->field($model, 'count')->input('number',['min'=>1,'value'=>1,'max'=>$product->count,'onchange'=>'$("#price").html(($(this).val() * '.$product->price.').toFixed(2))']) ?>
    <div>
        Текущая цена : <span id="price"><?=$product->price?></span>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Купить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
