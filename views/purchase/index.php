<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Совершенные покупки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'userID',
                'value' => function($member) {
                    $user = \app\models\User::findOne($member->userID);
                    return $user->login;
                },
            ],
            [
                'attribute' => 'productID',
                'value' => function($member) {
                    $product = \app\models\Product::findOne($member->productID);
                    return $product->name;
                }
            ],
            'count',
            'price',
        ],
    ]);
    ?>
</div>
